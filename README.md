# Find-Upsellees
This script will find students to upsell to (people with only one lesson left), and dump those people into a text file (in the /Data Analytics/meta/upsellee_script/ folder). Once you have the prerequisites ready just run "ruby get_upsellees.rb"

It takes a while to run (the rubyXL gem is very slow for some reason), but after it's done, it will spit out a file "New Upsells \*\*\*\*\*\*.txt" (where \*\*\*\*\*\* is the date); this contains information on what students have only one lesson left.

The meta folder containing the script **must** be put in the Data Analytics folder with all the data analytics sheets. When it runs, Google Drive may complain that a file (with a weird file name) is being deleted and that other people may be using it. Don't worry about this. The script makes its own copy of the data analytics sheet so it doesn't prevent other people from using it. It then deletes it.

# Prerequisites

###File Locations
The find_upsellees.rb file needs to be in the Data Analytics/meta/upsellee_script folder OR IT WILL FAIL

###Install Ruby
####Mac:
#####Using RVM
Go here: https://rvm.io/
Open terminal, and run the following commands:
```bash
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
curl -sSL https://get.rvm.io | bash -s stable
```

####Windows
#####Using Choco (better)
Open an administrative command prompt, and run the following command:

```bash			
@powershell -NoProfile -ExecutionPolicy Bypass -Command "iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))" && SET PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin
```

Once that's set up, run:
```bash
choco install ruby
```

#####Using RubyInstaller (lazier)
Just install the program here: http://rubyinstaller.org/

###Rubygems
Your installation of Ruby should come with rubygems. If not, there are instructions here: https://rubygems.org/pages/download

Once you have rubygems installed, run this on your cmd/terminal:
`gem install rubyXL`

###Running
You can now run the program by opening up terminal / command prompt and running "get_upsellees.rb" through ruby.
An easy way to get the file path is to drag the "find_upsellees.rb" file from your finder/explorer to the terminal/cmd
e.g. something like
#####Mac
`ruby Users/username/Google Drive/Data Analytics/meta/upsellee_script/get_upsellees.rb`

#####Windows		
`ruby "C:\Users\username\Google Drive\Data Analytics\meta\upsellee_script\get_upsellees.rb`

#Dependencies
This program looks for a file in the "Data Analytics" folder, and looks for the most recent file with a name starting with "Data Analytics EN." It will make a copy of this sheet for working purposes.

It then looks for the sheet called "Students." In this sheet, it looks for the column labelled "# of Remaining Sessions", **the label of which it assumes will be on the 7th row.** It goes through this column and looks for any students who have only 1 remaining session. When it finds one, it looks for the column labelled "English Name" (again assumed to be on the 7th row), and grabs that Name. It finally dumps the name into the file.
