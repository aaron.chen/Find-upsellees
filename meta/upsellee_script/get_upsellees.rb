#!/usr/bin/env ruby

require 'rubygems'
require 'rubyXL'

def find_column(sheet,name,label_row)
	for c in 0... sheet[label_row].size
		if sheet[label_row][c].value == name
			return c
		end
	end
end


this_absolute_path = File.expand_path(File.dirname(__FILE__))
Dir.chdir(this_absolute_path)
data_rel = "../../"

most_recent_sheets = []
most_recent_date = Time.new(1970, 1, 2)
filename_date_pos = 3

Dir.foreach(data_rel) do |sheet_filename|
	if (sheet_filename =~ /^Data Analytics EN*/)
		sheet_date_str = sheet_filename.split[filename_date_pos] #pay attention to this area if the file naming changes
		sheet_year, sheet_month, sheet_day = ["20"+sheet_date_str[4..5],sheet_date_str[0..1],sheet_date_str[2..3]].map{|s| s.to_i}
		sheet_date = Time.new(sheet_year, sheet_month, sheet_day)
		if sheet_date > most_recent_date
			most_recent_date = sheet_date
			most_recent_sheets = []
			most_recent_sheets << sheet_filename
		elsif sheet_date == most_recent_date
			most_recent_sheets << sheet_filename
		end
	end
end

most_recent_sheet = most_recent_sheets.max{|f1,f2| File.mtime(data_rel+f1) <=> File.mtime(data_rel+f2)}
working_copy = (Random.rand()*9999999999999999).to_i.to_s(16)
FileUtils.cp(data_rel+most_recent_sheet,"./#{working_copy}")

data = RubyXL::Parser.parse(data_rel+most_recent_sheet)
student_sheet = data['Students']
label_row = 6
remaining_sessions_col = find_column(student_sheet,"# of Remaining Sessions",label_row)
english_name_col = find_column(student_sheet,"English Name",label_row)

out = File.open("New Upsells #{most_recent_sheet.split[filename_date_pos]}.txt",'w')

student_sheet.each do |row|
	if row.nil? || row[remaining_sessions_col].nil?
		next
	elsif row[remaining_sessions_col].value == 1
		out.puts row[english_name_col].value
		out.puts
	end
end

FileUtils.rm("./#{working_copy}")
